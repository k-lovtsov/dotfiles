alias updatedb='sudo /usr/libexec/locate.updatedb'
autoload -Uz compinit && compinit
#source ~/.git-prompt.sh
#precmd () { __git_ps1 "%n" ":%~$ " "|%s" }

# The next line updates PATH for Yandex Cloud CLI.
if [ -f "$HOME/yandex-cloud/path.bash.inc" ]; then source "$HOME/yandex-cloud/path.bash.inc"; fi

# The next line enables shell command completion for yc.
if [ -f "$HOME/yandex-cloud/completion.zsh.inc" ]; then source "$HOME/yandex-cloud/completion.zsh.inc"; fi

# Enables terraform shell command completion
if [ -f "/opt/homebrew/bin/terraform" ]; then complete -o nospace -C /opt/homebrew/bin/terraform terraform; fi

PROMPT="[%w] [%T] [%F{green}%n%f@%F{magenta}%m%f]: %F{blue}%~%f
# "

export HISTFILE=~/.zsh_history
export HISTFILESIZE=10000
export HISTSIZE=10000
setopt INC_APPEND_HISTORY
export HISTTIMEFORMAT="[%F %T] "
setopt EXTENDED_HISTORY
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS

if [ -d "$HOME/.pyenv" ]
then
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
fi
