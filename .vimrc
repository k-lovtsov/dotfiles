set nocompatible
set background=dark

syntax on
set nu
set relativenumber
set mousehide
"set mouse=a
set mouse=""

filetype on
filetype plugin on

set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4
set autoindent
set showtabline=0

set wrap
set linebreak

set nobackup
set noswapfile

set encoding=utf-8
set ffs=unix,dos,mac
set fencs=utf-8,cp1251

set showmatch
set hlsearch
set incsearch
set ignorecase
